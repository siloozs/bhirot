
import React from "react";
import ReactDOM from "react-dom";
import html2canvas from 'html2canvas';
import { Base64 } from 'js-base64';
import $ from 'jquery';

import MemshalaFinalWrap from "./components/MemshalaFinalWrap/MemshalaFinalWrap";
import MoveToNextGame from "./components/MoveToNextGame/MoveToNextGame";
import BulidMemshala from "./components/BuildMemshala/BulidMemshala";
import GovermentList from "./components/GovermentList/GovermentList";
import Seats from "./components/Seats/Seats";
import Leader from "./components/Leader/Leader";
import OpenPage from "./components/OpenPage/OpenPage";
import Miflaga from "./components/Miflaga/Miflaga";
import Header from "./components/Header/Header"
import HeaderTitle from "./components/HeaderTitle/HeaderTitle"

//import { HashRouter as Router, Route } from "react-router-dom";

// import "./styles.scss";
import "./components/MemshalaFinalWrap/MemshalaFinalWrap.scss";
import "./components/BuildMemshala/BulidMemshala.scss";
import "./components/GovermentList/GovermentList.scss";
import "./components/Leader/Leader.scss";
import "./components/OpenPage/OpenPage.scss";
import './App.scss';
export default class App extends React.Component {
  state = {
    page: 1,
    SelectedLeaer: null,
    LeaderId: null,
    InSeats: false,
    coallition: null,
    ApiUrl:"https://qa-z.calcalist.co.il/mvc/short/2019/ElectionsIsrael"
  }
  //ApiUrl:"http://local.calcalist.co.il/ElectionsIsrael"
  //ApiUrl:"https://qa-z.calcalist.co.il/mvc/short/2019/ElectionsIsrael"

  //https://qa-z.calcalist.co.il/mvc/short/2019/ElectionsIsrael/api/Data/PostVotes
  //http://local.calcalist.co.il/                           ElectionsIsrael/api/Data/PostVotes
  ShareFF = (shareType,Gameindex) => {
    let CurrentDivToPring = Gameindex==1? "MiniWrapper" : 'CardWrapper';
    let sharedTemplateTitle = Gameindex==1? "זו הקואליציה שבחרתי" : 'זו הממשלה שהרכבתי';
    html2canvas(document.querySelector(`.${CurrentDivToPring}`)).then(canvas => {
      //document.body.appendChild(canvas)
      var MyCanvas = canvas.toDataURL("image/png");
      var CanvasToBased =Base64.encode(MyCanvas);
      console.log("CanvasToBased  "+CanvasToBased);
      var UserId =JSON.parse(localStorage.getItem("UserId"));
      $.ajax({
        type: "post",
        url: `${this.state.ApiUrl}/api/Image/CreateShareImage`,
        data: JSON.stringify({"_id":UserId,"_datauri": CanvasToBased,"_sharetype":String(Gameindex)}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          console.log(data);
          localStorage.setItem('UrlToShare', JSON.stringify(data));
        },
        failure: function (errMsg) {
          console.log(errMsg);
        }
      });
    });
    let sharedTemplateUrl;
    let UrlToShare=JSON.parse(localStorage.getItem("UrlToShare"));
    switch (shareType) {
      case 'facebook':
        sharedTemplateUrl = `https://m.facebook.com/sharer.php?u=${UrlToShare}`;
        window.open(sharedTemplateUrl);
        break;
      case 'whatsapp':
        //sharedTemplateUrl = `whatsapp://send?text=${sharedTemplateTitle}${String.fromCharCode(58, 32)}${UrlToShare}`;
        sharedTemplateUrl = `whatsapp://send?text=${UrlToShare}`;
        window.open(sharedTemplateUrl);
        break;
      case 'twitter':
        //sharedTemplateUrl = `https://twitter.com/share?url=${sharedTemplateBase}&text=${sharedTemplateTitle}`;
        sharedTemplateUrl = `https://twitter.com/share?url=${UrlToShare}&text=${sharedTemplateTitle}`;
        window.open(sharedTemplateUrl);
        break;
      default:
        break;
    }

  }
  changepage = (n, oData) => {
    //alert(n) //מספר הדף
    //alert(oData) //המפלגות שבחרתי
    if (n === 1 && oData) {
      //const { dataID } = oData;
      //this.setState({ page: n, SelectedLeaer: dataID });
    } else if (n === 2) {
      const { dataID } = oData;
      this.setState({ page: n, SelectedLeaer: oData, LeaderId: oData });
    } else if (n === 3) {
      this.setState({ page: n, coallition: oData });
    } else if (n === 10) {
      this.setState({ page: n, SelectedLeaer: oData });
    } else {
      this.setState({ page: n, SelectedLeaer: oData })
    }

  }

  render() {
    var dataLayer;
    dataLayer = [{ 'pageName': window.location.pathname + '?path=Election-2019' }];
    (function (w, d, s, l, i) {
      w[l] = w[l] || []; w[l].push({
        'gtm.start':
          new Date().getTime(), event: 'gtm.js'
      }); var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
          'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-NNZS59');
    // if (window.location.protocol != 'https:') {
    //   window.location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
    // }



    let currentPage = null;
    switch (this.state.page) {
      case 1:
        //currentPage =<MoveToNextGame/>
        currentPage = <Leader move={this.changepage} />;
        //currentPage = <BulidMemshala move={this.changepage} />;
        break;
      case 2:
        currentPage = <Seats move={this.changepage} leader={this.state.SelectedLeaer} InSeats={this.state.InSeats} Share={this.ShareFF} ApiUrl={this.state.ApiUrl}/>;
        break;
      case 3:
        currentPage = <MoveToNextGame move={this.changepage} data={this.state.SelectedLeaer} leader={this.state.LeaderId} coallition={this.state.coallition} />;
        break;
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
        currentPage = <BulidMemshala move={this.changepage} ApiUrl={this.state.ApiUrl}/>;
        break;
      case 12:
        localStorage.setItem('FinalRes', JSON.stringify(this.state.SelectedLeaer));
        currentPage = <MemshalaFinalWrap move={this.changepage} data={this.state.SelectedLeaer} leader={this.state.LeaderId} coallition={this.state.coallition} Share={this.ShareFF}/>;
        break;
      default:
        currentPage = <Leader move={this.changepage} />;
        break;
    }


    return (
      <div className="App">
        <HeaderTitle index={this.state.page} />
        <Header index={this.state.page} />
        {currentPage}
      </div>
    );
  }
}

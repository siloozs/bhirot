import React, { Component } from "react";
export default class OpenPage extends Component {
  constructor(){
    super();
    this.state = {};
  }
  moveTo = () => {
      this.props.move(1);
  }

  render() {
    return (
      <div className="OpenPage">
        <label class="CardWrapper">
          <input type="checkbox" />
          <div class="card">
            <div class="front">משחקי הקואליציה</div>
            <div class="back"> יייאיאיא</div>
          </div>
        </label>
      </div>
    );
  }
}

import React, { Component } from "react";
import MemshalaFinal from "../MemshalaFinal/MemshalaFinal";
import data from '../Data.json';
import Minister from '../Minister/Minister';
import $ from 'jquery';
export default class BulidMemshala extends Component {
  constructor() {
    super();
    this.state = {
      index: 0,
      insideIndex: 4,
      currentManImg: "avater.png",
      currentManName: '',
      currentManParty: '',
      selectedIdx: ''
    };
    //this.names = ['שר האוצר', 'משפטים', 'מדע', 'תרבות', 'ספורט', 'שיוויון חברתי', 'בטחון']
    this.names = ['שר האוצר', 'שר הבריאות', 'שר הרווחה', 'שר השיכון', 'שר החינוך', 'שר הכלכלה', 'שר הבטחון', 'שר החוץ'];
    this.EngName = ['JobOtzar', 'JobBrioot', 'JobRevaha', 'JobShikoon', 'JobHinuh', 'JobCalcala', 'JobBitahoon', 'JobHooz'];
    this.keys1 = {

    }
    this.ministers = {

    }
    this.Engministers = {

    }
    this.wrapperID = 0;
    this.selectedIx = 0;
    this.parentIdxGroup = 0;
  }
  next = () => {
    const { index } = this.state;
    const ministerName = this.names[this.state.index];
    if (!this.ministers[ministerName]) {
      alert(`חייב לבחור את ${ministerName}`);
      return;
    }
    this.props.move(this.state.insideIndex + 1);
    if (index == 7) {
      let arr = Object.keys(this.ministers).map((k) => this.ministers[k]);
      let Engarr = Object.keys(this.Engministers).map((k) => this.Engministers[k]);
      
      //console.log("ENG --- >>>> " + JSON.stringify(Engarr));

      var newobj = {};
      var UserId =JSON.parse(localStorage.getItem("UserId"))
      newobj["id"]=UserId;
      Engarr.forEach(function (key) {
        //console.log(Object.keys(key) + " - > " + Object.values(key));
        var ObjectKey = Object.keys(key);
        var ObjectValue = Object.values(key);
        newobj[ObjectKey] = (ObjectValue)[0];
      })
      //console.log("the new obj ---> "+JSON.stringify(newobj))
      $.ajax({
        type: "post",
        url: `${this.props.ApiUrl}/api/Data/PostRoles`,
        //http://qa-z.calcalist.co.il/mvc/short/2019/ElectionsIsrael/Client/Index.html
        //url: baseUrl + 'Registration/Post' + view.charAt(0).toUpperCase() + view.slice(1) + '.aspx',
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(newobj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          console.log(data);
        },
        failure: function (errMsg) {
          console.log(errMsg);
        }
      });
      this.props.move(12, arr);
    }
    this.selectedIdx = 0;
    //place the avate image
    this.currentManImg = 'avater.png';
    this.currentManName = '';
    this.currentManParty = '';
    if (this.ministers[this.names[this.state.index + 1]]) {
      this.selectedIdx = this.ministers[this.names[this.state.index + 1]].selectedIdx;
      // console.log("--->> " + this.ministers[this.names[this.state.index + 1]]);
      // console.log("---2>> " + this.ministers[this.names[this.state.index + 1]].selectedIdx);
      this.currentManImg = this.ministers[this.names[this.state.index + 1]].Image;
      this.currentManName = this.ministers[this.names[this.state.index + 1]].firstname;
      this.currentManParty = this.ministers[this.names[this.state.index + 1]].party;
    }
    this.setState(prevState => ({ index: prevState.index + 1, insideIndex: this.state.insideIndex + 1, selectedIdx: this.selectedIdx, currentManImg: this.currentManImg, currentManName: this.currentManName, currentManParty: this.currentManParty }))
  }
  prev = () => {
    if (this.ministers[this.names[this.state.index - 1]]) {
      this.selectedIdx = this.ministers[this.names[this.state.index - 1]].selectedIdx;
      this.currentManImg = this.ministers[this.names[this.state.index - 1]].Image;
      this.currentManName = this.ministers[this.names[this.state.index - 1]].firstname;
      this.currentManParty = this.ministers[this.names[this.state.index - 1]].party;
    } else {
      //console.log('Wron---this.selectedIx ===> ', this.selectedIdx)
    }
    this.props.move(this.state.insideIndex - 1)
    this.setState(prevState => ({ index: prevState.index - 1, selectedIdx: this.selectedIdx, insideIndex: this.state.insideIndex - 1, currentManImg: this.currentManImg, currentManName: this.currentManName, currentManParty: this.currentManParty }));
  }

  selectedMinister = (e, parentIdxGroup) => {
    this.parentIdxGroup = parentIdxGroup;
    const ministerName = this.names[this.state.index];
    const ministerEngName = this.EngName[this.state.index];
    if (data[e.target.dataset.party]) {
      const currentParty = data[e.target.dataset.party];
      const currentImage = e.target.dataset.img;
      const currentName = e.target.dataset.name;
      const currentMParty = currentParty.PartyName;
      const wrapperID = parseInt(e.target.parentElement.dataset.box);
      const selectedIdx = e.target.dataset.selected;
     // console.log('selectedIdx   ', selectedIdx);
      this.ministers[ministerName] = {
        'jobname': ministerName,
        'firstname': e.target.dataset.name,
        'party': currentParty.PartyName,
        'Image': e.target.dataset.img,
        'selectedIdx': selectedIdx
      }
      this.Engministers[ministerEngName] = {
        [ministerEngName]: e.target.dataset.engname
      }
      //this.Engministers[ministerEngName] = JSON.stringify({ministerEngName: e.target.dataset.engname});
      //console.log(e.target.dataset)
      this.selectedIx = selectedIdx;
      this.setState({ selectedIdx: selectedIdx, currentManImg: currentImage, currentManName: currentName, currentManParty: currentMParty })
     // console.table(this.ministers);
    }
  }
  render() {
    var storedNames = JSON.parse(localStorage.getItem("ChosenMiflagot"));
    return (
      <div className="BulidMemshala">
        <div className="Btns">
          <span className="SarTitle">{this.names[this.state.index]}</span>
          <span className="next" onClick={this.next}>&lt;</span>
          <span className="Prev" onClick={this.prev}> {this.state.index ? ">" : ""} </span>
        </div>
        <div className="JustTxt">לחצו כדי לבחור מועמד להרכבת הממשלה</div>
        <div className="TopLeftSide">
          <div className="ManSelectedJob" style={{ backgroundImage: `url(./static/images/${this.state.currentManImg})` }}></div>
          <div class="CurrentManName">{this.state.currentManName}</div>
          <div class="CurrentManParty">{this.state.currentManParty}</div>
        </div>
        <div className="RolsWrapper">
          <div className="JustTxt BottomTxt">לחצו כדי לבחור מועמד</div>
          <div className="FaqWrapper">
            {
              data.map((item, index) => {
                //console.log("storedNames[index] ==> "+storedNames[index]);
                return storedNames[index] ? <Minister key={index} click={this.selectedMinister} index={index} name={item.PartyName} Members={item.Members} group={this.parentIdxGroup} current={this.state.selectedIdx} /> : null;
              }
              )
            }
          </div>
        </div>
      </div>
    );
  }
}

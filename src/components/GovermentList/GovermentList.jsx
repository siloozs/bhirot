import React, { Component } from "react";
import Miflaga from "../Miflaga/Miflaga";
import data from '../Data.json'
export default class GovermentList extends Component {
  state = {};
  render() {
    return (
      <div className="GovermentList">
        <div className="MiflagaList">
          {
            data.map((item, index) => {
            return (
              <Miflaga key={index} name={item.PartyName} mandat={item.PartyMandats} />
            )
          })}
        </div>
      </div>
    );
  }
}

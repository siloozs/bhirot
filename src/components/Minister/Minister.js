import React, { Component } from "react";
import $ from 'jquery';
export default class Minister extends Component {
    constructor(){
        super();
        this.state = {
            selected: false
        };
        this.clickedMinisterID = 0;
    }
    changeSelection1 = () => {
        const selected = !this.state.selected;
        this.setState({ selected });
    }
    aaa = (labelID) => {
        var el = document.getElementsByClassName(`lbl-${labelID}`);
        el.parentNode.classList.add('active');
        return;
        if ($(this).is(':checked')) {
            this.parentNode.classList.add('active');
        } else {
            this.parentNode.classList.remove('active');
        }
    }
    render() {
        const { selected } = this.state;
        const cls = selected ? 'active' : '';
        const groupIdx = this.props.index;
        const current = this.props.current;
        return (
            <div class="faq" data-box={this.props.index}>
                <input type="checkbox" class={`lbl-${this.props.index}`} id={this.props.index} />
                <label htmlFor={this.props.index}>{this.props.name}</label>
                {
                    this.props.Members.map((item, index) => (
                            <p data-currrrent={current} data-cc={index + '_' + groupIdx} class={`moama m-${index} ${(index + '_' + groupIdx) === current  ? 'active' : ''}`} data-img={item.img} data-selected={index + '_' + groupIdx} onClick={ (e) => this.props.click(e , this.props.index)} data-party={this.props.index} key={index} data-Engname={item.EngName} data-name={item.Name}>{item.Name}</p>
                    ))
                }
                <span class="AroowBack" />
            </div>
        );
    }
}
/*
 <div className="wrapperMinister">
                            <input type="checkbox" id={this.props.group +"_" + index}/>
                            <label for={this.props.group +"_" + index} />
  </div>
*/
// (this.props.current === index && this.props.group == this.props.index ? `${cls}` : '')

import React, { Component } from "react";
import './Miflaga.scss';
export default class Miflaga extends Component {
  state = {
    selected: false
  };
   
  changeSelection = () => {
      // const  selected  = !this.state.selected;   
      this.props.select(this.props.index);
      // this.setState({selected});
  }
  
  render() {
    const { selected } = this.state; 
    // let cls = selected ? 'active' : '';
    // //console.log("sdfsdf   "+this.props.SelectedLeader);
    // if (this.props.SelectedLeader=="Bibi" && this.props.name=="ליכוד" || this.props.SelectedLeader=="Gantz" && this.props.name=="חוסן לישראל") {
    //    cls ='active';
    // }
    let cls = this.props.selected || this.props.SelectedLeader=="Bibi" && this.props.name=="הליכוד" || this.props.SelectedLeader=="Gantz" && this.props.name=="כחול לבן" ? 'active' : ''
    return (
      <div className={`Miflaga ${cls}`} data-name={this.props.name} onClick={this.changeSelection}>
        <div className="MiflagaName" data-mandat={this.props.mandat}>{this.props.name}</div>
        <span
          id="AroowBack"
          className="AroowBack"
          data-mandat={this.props.mandat}
        />
      </div>
    );
  }
}

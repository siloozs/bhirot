import React from 'react';
import "./HeaderTitle.scss"
const HeaderTitle = (props) => {
    var PageTitle = (props.index < 3)?("מרכיבים קואליציה"):((props.index >= 3 && props.index < 12)?"בונים קבינט כלכלי-חברתי":"הקבינט הכלכלי-חברתי שבחרתם");
    return (
        <div className="HeaderTitle ShualBold">
            <div class="Calc"><span class="HeadrImg"></span></div>
            <span>{PageTitle}</span>
        </div>
    )
}
export default HeaderTitle;

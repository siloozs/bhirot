import React from 'react';
import "./Header.scss";
const Header = (props) => {
    return (
        <div className="Header ShualBold">
            <div class="HeaderProgress">
                <div class="ProgressLine" style={{width: props.index*12 + "%"}}></div>
            </div>
            <span class="PageNumber">{props.index}/12</span>
        </div>
    )
}


export default Header;

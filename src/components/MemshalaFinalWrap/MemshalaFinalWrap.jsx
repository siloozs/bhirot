
import React, { Component } from "react";
import MemshalaFinal from "../MemshalaFinal/MemshalaFinal";
import "../MemshalaFinalWrap/MemshalaFinalWrap.scss";
import "../MoveToNextGame/MoveToNextGame.scss";
import html2canvas from 'html2canvas';
import { Base64 } from 'js-base64';
import $ from 'jquery';
//Html to png --Canvas
//https://stackoverflow.com/questions/18312271/how-to-save-a-rendered-webpage-as-image-with-javascript
export default class MemshalaFinalWrap extends Component {
  constructor() {
    super();
    this.state = {
      FinalJobs: []
    }
  }
  componentDidMount() {
    var Jobs = this.props.data;

  }
  renderMemshala() {
  }
  Sharing = (ShareType) => {
    this.props.Share(ShareType,2);
  }
  render() {
    var count = 0;
    var mandtasFinal = this.props.coallition;
    for (var key in mandtasFinal) {
      if (mandtasFinal.hasOwnProperty(key)) {
        count += parseInt(mandtasFinal[key].mandtas);
      }
    }
    var Name = 'בנימין נתניהו';
    var Miflaga = 'הליכוד'
    var LeaderClass = 'Bibi'
    if (this.props.leader == 1) {
      Name = 'בני גנץ'
      Miflaga = 'כחול לבן'
      LeaderClass = 'Gantz'
    }

    let output;
    const jobs = this.props.data;


    return (
      <div className="FinalGameMemshala">
        <label class="CardWrapper">
          <input type="checkbox" />
          <div class="card">
            <div className="CardFront front">
              <div className="RoshMemshala">
                <div class="plan plan1">
                  <p className={`RoshMem manImg ${LeaderClass}`}></p>
                  <span className="Title">ראש הממשלה</span>
                  <span className="Name">{Name}</span>
                  <span className="Miflaga">{Miflaga}</span>
                </div>
              </div>
              <div className="TopJobs">
                <MemshalaFinal key={1} item={jobs[0]} />
                <MemshalaFinal key={2} item={jobs[1]} />
              </div>
              <div className="BottomJobs">
                {
                  jobs.map((item, index) => {
                    //FinalAll.push(item);
                    //console.log("FinalAll "+JSON.stringify((FinalAll)));
                    return index > 1 ? <MemshalaFinal key={index} item={item} /> : null;
                  })
                }
              </div>
            </div>
            {/* <div className="back" onClick={this.NextGame}><p className="red">נראה מבטיח!</p>
              שתפו את הממשלה שהרכבתם
              <div class="Sharer">
                <span className="Icon whatsapp" onClick={() => this.Sharing('whatsapp')}></span>
                <span className="Icon Facebook" onClick={() => this.Sharing('facebook')}></span>
                <span className="Icon Twiiter" onClick={() => this.Sharing('twitter')}></span>
              </div>
            </div> */}
          </div>
        </label>
        <div className="MobileBack back" onClick={this.NextGame}><p className="red">נראה מבטיח!</p>
              שתפו את הממשלה שהרכבתם
              <div class="Sharer">
                <span className="Icon whatsapp" onClick={() => this.Sharing('whatsapp')}></span>
                <span className="Icon Facebook" onClick={() => this.Sharing('facebook')}></span>
                <span className="Icon Twiiter" onClick={() => this.Sharing('twitter')}></span>
              </div>
        </div>
      </div >
    );
  }
}

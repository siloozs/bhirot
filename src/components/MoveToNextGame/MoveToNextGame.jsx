
import React, { Component } from "react";
import MemshalaFinal from "../MemshalaFinal/MemshalaFinal";
import "../MoveToNextGame/MoveToNextGame.scss";
//Html to png --Canvas
//https://stackoverflow.com/questions/18312271/how-to-save-a-rendered-webpage-as-image-with-javascript
export default class MoveToNextGame extends Component {
    constructor() {
        super();
        this.state = {
            FinalJobs: []
        }
    }
    componentDidMount() {
        var Jobs = this.props.data;
        var count = 0;
        var mandtasFinal = this.props.coallition;

        if (typeof Jobs === "object") {
            this.setState({
                FinalJobs: Object.keys(Jobs).map(function (key) {
                    return [parseInt(key), Jobs[key]];
                })
            })
        }

    }
    renderMemshala() {
        var MName = 'בנימין נתניהו';
        var MMiflaga = 'הליכוד'
        if (this.props.leader == 1 || this.props.leader == 2) {
            MName = 'בני גנץ'
            MMiflaga = 'כחול לבן'
        }
        var FinalAll = [{
            "jobname": "ראש ממשלה",
            "firstname": MName,
            "party": MMiflaga,
            "Image": "b1.png",
            "selectedIdx": "0_1"
        }];
        if (this.state.FinalJobs.length < 1 || !this.state.FinalJobs) {
            return;
        }
        //return this.state.FinalJobs.map((item, index) => <MemshalaFinal key={index} {...item} />);
        return this.state.FinalJobs.map(function (item, index) {
            //FinalAll.push(`index-${index}:`)
            //FinalAll.push(`index-${index}`);
            //var IndexedObj =`index-${index}`+item[1]
            FinalAll.push(Object.assign({}, item[1]));
            //FinalAll.push(Object.assign({},IndexedObj));
            localStorage.setItem('FinalAll', JSON.stringify(FinalAll));
            return <MemshalaFinal key={index} {...item} />
        })
    }
    NextGame = () => {
        this.props.move(4);
    }
    render() {
        //    var FinalAll=[];
        var count = 0;
        var mandtasFinal = this.props.coallition;
        for (var key in mandtasFinal) {
            if (mandtasFinal.hasOwnProperty(key)) {
                count += parseInt(mandtasFinal[key].mandtas);
            }
        }
        var Name = 'ביבי';
        var Miflaga = 'הליכוד'
        var LeaderClass = 'Bibi'
        if (this.props.leader == 1) {
            Name = 'גנץ'
            Miflaga = 'כחול לבן'
            LeaderClass = 'Gantz'
        }
        return (
            <div className="MoveToNextGame" onClick={this.NextGame}>
                <div className="FinalMemshala">
                    {this.renderMemshala()}
                    <div className="RoshMemshala">
                        <div className="plan plan1">
                            {/* <h1 className="MemshalaFinalh1">{this.props[1].firstname}</h1> */}
                            <p className={`manImg ${LeaderClass}`}></p>
                            <span className="Title">ראש הממשלה</span>
                            <span className="Name">{Name}</span>
                            <span className="Miflaga">{Miflaga}</span>
                        </div>
                    </div>
                    <div className="TopJobs">
                        <div className="SarDetails plan1"><p className="manImg"></p><span className="Title">שר האוצר</span><span className="Name"></span><span className="Miflaga"></span></div>
                        <div className="SarDetails plan1"><p className="manImg"></p><span className="Title">שר הבריאות</span><span className="Name"></span><span className="Miflaga"></span></div>
                        
                    </div>
                    <div className="BottomJobs">
                        <div className="SarDetails plan1"><p className="manImg"></p><span className="Title">שר הרווחה</span><span className="Name"></span><span className="Miflaga"></span></div>
                        <div className="SarDetails plan1"><p className="manImg"></p><span className="Title">שר השיכון</span><span className="Name"></span><span className="Miflaga"></span></div>
                        <div className="SarDetails plan1"><p className="manImg"></p><span className="Title">שר החינוך</span><span className="Name"></span><span className="Miflaga"></span></div>
                        <div className="SarDetails plan1"><p className="manImg"></p><span className="Title">שר הכלכלה</span><span className="Name"></span><span className="Miflaga"></span></div>
                        <div className="SarDetails plan1"><p className="manImg"></p><span className="Title">שר הבטחון</span><span className="Name"></span><span className="Miflaga"></span></div>
                        <div className="SarDetails plan1"><p className="manImg"></p><span className="Title">שר החוץ</span><span className="Name"></span><span className="Miflaga"></span></div>
                    </div>
                </div>
                <div className="MoveTo Alef"> בחרו בשרים שיקבלו את התיקים המרכזיים <span className="FuncioArrow"> &#187; </span></div>
            </div>
        );
    }
}

import React, { Component } from "react";
import Seats from "../Seats/Seats";
import "../Seats/Seats.scss";
//const rootElement = document.getElementById("Man");
// rootElement.addEventListener("click", displayDate);

export default class Leader extends Component {
  state = {
    PrimeMinister: null,
    selected: 0
  };
  //https://stackoverflow.com/questions/24502898/show-or-hide-element-in-react
  moveTo = (dataID) => {
    this.setState({ PrimeMinister: dataID });
    //this.select(this.index);
    this.setState({ selected: dataID });
    // this.setState({ PrimeMinister: dataID }, () => {
    //   console.log(this.state.Leaderss, 'Leaderss');
    // }); 
    setTimeout(function() { //Start the timer
      this.MoveNextPage()
  }.bind(this), 100)
  }
  MoveNextPage = (dataID) => {
    if (!this.state.selected) {
      alert('יש לבחור ראש ממשלה');
      return;
    }
    this.props.move(2, this.state.PrimeMinister);
  }
  render(props) {
    const { selected } = this.state;
    const cls = selected ? 'active' : '';
    return (
      <div className="Leader">
        <div className="LeaderSeats">
          <Seats />
          {/* <span class="NextBtn" onClick={this.MoveNextPage}>&gt;</span> */}
        </div>
        <div className="ChooseLeader">
          <span class="RandomTxt Alef">הסקרים כבר מראים תמונה די ברורה,<br /> הגיע הזמן להרכיב קואליציה<br />
            <span class="red">*</span>
            <br />
            בחרו במועמד שלכם לעמוד בראשה
      </span>
          <div className="RoshMemshalaWrp">
            <span index={1} className={`Man ShualRegular Gantz ` + (selected === 1 ? `${cls}` : '')} data-id="1" onClick={() => this.moveTo(1, this)}>כחול לבן</span>
            <span index={2} className={`Man ShualRegular Bibi ` + (selected === 2 ? `${cls}` : '')} data-id="2" onClick={() => this.moveTo(2, this)}>הליכוד</span>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import ReactDOM from "react-dom";
import $ from 'jquery';
import GovermentList from "../GovermentList/GovermentList";
import { Redirect } from "react-router-dom";
import { S_IFIFO } from "constants";
import data from '../Data.json';
import Miflaga from "../Miflaga/Miflaga";
import "./Seats.scss";
//let wrapper = document.getElementById("wrapper");


var CounterMandat = 0;
var myHTML = "";
var length;
for (var y = 1; y <= 3; y++) {
  myHTML += `<div id="container" class="container cont-${y}">`;
  for (var x = 1; x <= 4; x++) {
    myHTML += `<div class="col-${x}">`;
    switch (x) {
      case 1:
        length = 13;
        break;
      case 2:
        length = 11;
        break;
      case 3:
        length = 9;
        break;
      case 4:
        length = 7;
        break;
      default:
    }
    for (var i = 1; i <= length; i++) {
      myHTML += '<div class="seat">&nbsp;</div>';
    }
    myHTML += "</div>";
  }
  myHTML += "</div>";
}
export default class Seats extends Component {
  constructor() {
    super();
    this.state = {
      SelectedLeader: "",
      PrimeMinisterPartyName: "",
      isLoading: true,
      MandataNumber: 0,
      Miflagot: [],
      SharingPage: false,
      ShareActive: "",
      ShareCoalizia: "",
      finish: false
    };
    this.data = data;
    this.finalResult = { coallition: {}, oposition: [] };
    this.collationClasses = {};
    // window.fetch('./Data.json')
    // .then(data => data.json())
    // .then(data => this.data = data);
  }
  componentDidMount() {
    document.getElementsByClassName("IsCoalition")[0].innerHTML = '0';
    CounterMandat = 0;
    var that = this;
    var LeaderClass;

    var wrapper = document.querySelector(".Wrapper");
    wrapper.innerHTML = myHTML;
    //var element = document.querySelector('App');
    // element.classList.contains('Leader');
    //alert(element.classList.contains('Leader'));
    //If we are in page-1 Dont Show spinner
    if (document.querySelectorAll('.App .Leader').length > 0) {
      this.setState({
        isLoading: false
      });
    } else {
      if (this.props.leader == 1) {
        this.setState({ SelectedLeader: "Gantz", PrimeMinisterPartyName: "כחול לבן" })
        this.selectedParty(7)
      } else {
        this.selectedParty(1)
        this.setState({ SelectedLeader: "Bibi", PrimeMinisterPartyName: "הליכוד" })
        //$('[data-name="ליכוד"]').click();
      }
    }
    setTimeout(() => {
      this.setState({
        isLoading: false
      });
    }, 0);
  }
  handleLoading = isLoading => {
    this.setState({ isLoading: isLoading, loadingClass: "" });
  };
  sss() {
    var testimonials = document.querySelectorAll('.applayBackgroundAnimation');
    Array.prototype.forEach.call(testimonials, function (elements, index) {
      elements.classList.remove('applayBackgroundAnimation');
    });
  }
  Prev = () => {
    this.props.move(1);
  }
  Sharing = (ShareType) => {
    this.props.Share(ShareType,1);
  }
  buildMemshala = () => {
    if (this.state.MandataNumber > 60) {
      const collaitionObj = this.finalResult.coallition;
      //The Object.keys() method returns an array of a given object's own property names, in the same order as we get with a normal loop.
      //from object to arrray
      let coallitionArr = [{
        [this.state.SelectedLeader]: this.state.SelectedLeader,
        "PrimeMinisterPartyName": this.state.PrimeMinisterPartyName,
        "MandatesSummary": `${this.state.MandataNumber}`
      }]
      coallitionArr.push(Object.keys(this.finalResult.coallition).map(item => ({
        [collaitionObj[item].PartyName]: collaitionObj[item].PartyName, 'mandtas': collaitionObj[item].PartyMandats
      }))
      );

      let ChosenMiflagot = [];
      let ChosenMiflagotNames = [];
      ChosenMiflagotNames.push(this.state.SelectedLeader);
      for (var i in this.finalResult.coallition) {
        ChosenMiflagot[parseInt(this.finalResult.coallition[i].Id) - 1] = this.finalResult.coallition[i].PartyEngName;
        ChosenMiflagotNames.push(this.finalResult.coallition[i].PartyEngName);
      }
      if (!this.state.SharingPage) {
        this.setState({ ShareActive: "ShareActive", ShareCoalizia: "ShareCoalizia", SharingPage: "true" }, () => {
          $.ajax({
            type: "post",
            url: `${this.props.ApiUrl}/api/Data/PostVotes`,
            data: JSON.stringify({ "_data": ChosenMiflagotNames }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
              console.log("data => "+data);
              localStorage.setItem("UserId",data);
            },
            failure: function (errMsg) {
              console.log(errMsg);
            }
          });
        });
        localStorage.setItem('coallitionArr', JSON.stringify(coallitionArr));
        localStorage.setItem("ChosenMiflagot", JSON.stringify(ChosenMiflagot));
        localStorage.setItem("ChosenMiflagotNames", JSON.stringify(ChosenMiflagotNames));
        return;
      }
    } else {
      alert(`אין לכם עדיין קואליציה,מינימום 60 מנדטים`);
    }
  }

  selectedParty = (index) => {
    var selectedSeats = Number(this.data[index].PartyMandats);
    //alert(selectedSeats)
    if (selectedSeats==0) {
      alert('לא ניתן לבחור מפלגה עם 0 מנדטים');
      return;
    }
    //console.log("index -->" + index);
    //console.log("SelectedLeader -> ",this.state.SelectedLeader);
    var InSeats = this.props.InSeats
    InSeats = true;
    if (InSeats) {
      if (index == 1 && this.state.SelectedLeader == "Bibi" || index == 7 && this.state.SelectedLeader == "Gantz") {
        alert('לא ניתן למחוק מפלגה של ראש ממשלה')
        return;
      }
    }
    var counter = 0;
    var IsCoalition = document.getElementsByClassName("IsCoalition");
    var Seats = document.getElementsByClassName("Seat");
    counter += selectedSeats;
    //this.classList.add("Chosen");
    //IsCoalition[0].innerHTML = counter
    const PartyName = this.data[index].PartyName;
    let delSeats = false;
    if (this.finalResult.coallition[PartyName]) {
      delete this.finalResult.coallition[PartyName];
      delSeats = true;
      //console.log('removed  miflaga --> ' , this.data[index]);
      CounterMandat -= parseInt(this.data[index].PartyMandats);
      IsCoalition[0].innerHTML = `<div class="IscoaNumWrp"><span class="IscoaNum">${CounterMandat}</span>מנדטים</div>`
      //console.log("CounterMandat ==>> " + CounterMandat);
    } else {
      this.finalResult.coallition[PartyName] = this.data[index];
      CounterMandat += parseInt(this.data[index].PartyMandats);
      IsCoalition[0].innerHTML =`<div class="IscoaNumWrp"><span class="IscoaNum">${CounterMandat}</span>מנדטים</div>`
      //console.log("CounterMandat ==>> " + CounterMandat);
    }
    if (CounterMandat > 60) {
      IsCoalition[0].innerHTML = `יש קואליציה <span class="IscoaNum">${CounterMandat}</span>`
      IsCoalition[0].classList.add("CoalitionActive");
    } else {
      IsCoalition[0].classList.remove("CoalitionActive");
    }
    this.setState({ MandataNumber: CounterMandat });

    if (counter < 120) {
      let currentMandats = parseInt(this.data[index].PartyMandats);
      if (delSeats) {
        let counterDeleted = 0;
        let that = this;

        let seatsElements = document.querySelectorAll('.seat.applayBackgroundAnimation');
        let lenElems = seatsElements.length;
        for (let i = lenElems - 1; i => 0; i--) {
          seatsElements[i].classList.remove('applayBackgroundAnimation');
          counterDeleted++;
          if (counterDeleted === that.collationClasses[PartyName].mandats) {
            break;
          }
        }

      } else {
        this.collationClasses[PartyName] = {
          'index': document.querySelectorAll(".seat.applayBackgroundAnimation").length,
          'mandats': currentMandats
        };
        $(".seat:not(.applayBackgroundAnimation)").slice(0, selectedSeats).addClass('applayBackgroundAnimation');
      }


    }

  } //END => selectedParty
  profileSpinner = "spinner";
  NextGame = () => {
    var coallitionList =JSON.parse(localStorage.getItem("coallitionArr"));
    this.props.move(3, coallitionList);
  }
  reset = () => {
    let seatsElements = document.querySelectorAll('.Miflaga');
    let lenElems = seatsElements.length;
    for (let i = lenElems - 1; i >= 0; i--) {
      seatsElements[i].classList.remove('active');
    }
    let newObj = {};
    let partyNamePrimeMinister = this.state.PrimeMinisterPartyName;

    newObj[partyNamePrimeMinister] = this.data.find(item => item.PartyName === partyNamePrimeMinister);
    this.finalResult.coallition = null;
    this.finalResult.coallition = newObj;
    CounterMandat = parseInt(newObj[partyNamePrimeMinister].PartyMandats);
    var IsCoalition = document.getElementsByClassName("IsCoalition");
    IsCoalition[0].innerHTML = `<div class="IscoaNumWrp"><span class="IscoaNum">${CounterMandat}</span>מנדטים</div>`
    IsCoalition[0].classList.remove("CoalitionActive");
    document.querySelector('.Miflaga[data-name="' + partyNamePrimeMinister + '"]').classList.add('active');

    let counterDeleted = 0;
    seatsElements = document.querySelectorAll('.seat.applayBackgroundAnimation');
    lenElems = seatsElements.length;
    for (let i = lenElems - 1; i >= CounterMandat; i--) {
      seatsElements[i].classList.remove('applayBackgroundAnimation');
    }
    this.setState({ MandataNumber: CounterMandat })
  }
  render() {
    return (
      <div className={`Seats ${this.state.isLoading ? this.profileSpinner : ""}`}>
        <div className="MiniWrapper">
          <div id="PageWrapper" className="PageWrapper">
            <div className="SeatsDetails">
              <span className="CaolitionText">קואליציה</span>
              <span className="CoaltionNum">{this.state.MandataNumber}</span>
              <span className="CaolitionText">אופוזיציה</span>
              <span className="CoaltionNum Op">{120 -this.state.MandataNumber}</span>
              <span className="">מנדטים</span>
              <span className="TotalMandats">120</span>
            </div>
            <div id="Wrapper" className="Wrapper">
              <div id="container" className="container cont-1" />
              <div id="container" className="container cont-2" />
              <div id="container" className="cont-3" />
            </div>
            <div className="IsCoalition"></div>
            {/* <div className={"SelectedLeader " + this.state.SelectedLeader}></div> */}
            {/* <img src="/static/media/Bibi1_1.b8ab4563.png" className={"SelectedLeader " + this.state.SelectedLeader} /> */}
            <div className={"SelectedLeader " + this.state.SelectedLeader}></div>
            {/* <div className="IsCoalition aa"></div> */}
          </div>
          <div className="Buttons">
            {/* {this.state.MandataNumber}
            {this.state.Miflagot} */}
            <span className={"Continue Alef " + this.state.ShareActive} onClick={this.buildMemshala}>הבא</span>
            <span onClick={this.reset} className={"Restart " + this.state.ShareActive}></span>
            <span className={"Prev Alef " + this.state.ShareActive} onClick={this.Prev}>הקודם</span>
          </div>
        </div>
        <div className={"ShareCoalizion " + this.state.ShareCoalizia}>
          <div class="Rightwrp">
            <p class="red">הצלחתם !</p><p class="Kavod">כל הכבוד </p>
            <span>שתפו את <br />הקואליציה שהרכבתם</span>
            <div class="Sharer">
              <span className="Icon whatsapp" onClick={() => this.Sharing('whatsapp')}></span>
              <span className="Icon Facebook" onClick={() => this.Sharing('facebook')}></span>
              <span className="Icon Twiiter" onClick={() => this.Sharing('twitter')}></span>
            </div>
          </div>
          <div class={"Leftwrp " + this.state.ShareActive} onClick={this.NextGame}><p class="Tikim">עכשיו נראה אתכם מחלקים תיקים <span class="FuncioArrow">&#187;</span></p>
          </div>
        </div>
        <div className={"GovermentList " + this.state.ShareActive}>
          <span className="Explain Alef">נראה אתכם מגיעים ל-61 <br/></span>
          <div className="MiflagaList">
            {
              data.map((item, index) => {
                return (
                  <Miflaga selected={this.finalResult.coallition[item.PartyName] ? true : false} key={index} index={index} select={this.selectedParty} name={item.PartyName} mandat={item.PartyMandats} SelectedLeader={this.state.SelectedLeader} />
                )
              })}
             <div className="SekerText"><span className="Memoza"><span className="red">*</span>מספר המנדטים מבוסס על ממוצע הסקרים האחרונים </span></div>
          </div>
        </div>
      </div>
    );
  }
}
document.addEventListener("DOMContentLoaded", function (event) {
  let Coalitiontrue = document.getElementsByClassName('IsCoalition');
  //Coalitiontrue.classList.add('good');
});

